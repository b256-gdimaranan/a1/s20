
let number = prompt("give me a number");
console.log("the number you provide is:" + number);

for (let i = number; i >= 0; i--) {
    if (i <= 50) {
        console.log("The current value is at "+ i + ". Terminating the loop.")
        break;
    }
    if (i % 10 == 0) {
        console.log("The number is divisible by 10. Skipping the number.");
    }
    else if (i % 5 == 0) {
        console.log(i);
    }
}

let str = "supercalifragilisticexpialidocious";
let consonants = ""

for (let i = 0; i < str.length; i++) {
    if(str[i] == "a" || str[i] == "e" || str[i] == "i" ||
        str[i] == "o" || str[i] == "u") {
        continue;
    }
    consonants = consonants + str[i]
}
console.log(str);
console.log(consonants);